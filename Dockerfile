FROM golang:1
LABEL MAINTAINER="Simon Ball<simon.ball@aencoin.com>"

ARG BUILD_ID=LocalRun

WORKDIR /go/src/app
COPY . .

RUN go get \
    github.com/tools/godep

RUN godep go build
RUN go build -o aen.agent && cp aen.agent $GOPATH/bin/aen.agent

# Change the build number in the ini if possible
RUN sed -i "s/LocalRun/$BUILD_ID/g" /go/src/app/resources/config-agent.ini.twig

CMD ["aen.agent"]
