# Chain-Agent

This Agent sits in every node, and is responsible for reporting the state and performance of the network, and to identify any potential issues or problems.

## Prerequisites

Either [**Docker**](https://docs.docker.com/install/) or [**GoLang**](https://golang.org/doc/install) environment

Install the agent with either of the following commands, depending on whether you are using Docker or Native

```
# Native 

# Compiles and puts the binary in your GOPATH binary folder
go install
# Just builds the binary
go build -o aen.agent

# Docker

# Build the Docker image
docker build -t aenco/chain-agent .
```

## Usage

The agent can run without any parameters but, will expect to find a set of resource files (from the configurator) in any of the following paths
```
../resources
~/.aen/resources
./resources
```

In the case of the Docker image, the home path is `/root` and the working path (if using resources in the relative directory) is located at `/go/bin/aen.agent`

The following arguments are also available for use

```
-d string
    Enable Debug
-id string
    Force the Node ID
-lat string
    Latitude
-lon string
    Longitude
-name string
    Force the Node Name
```