package main

import (
	"flag"
)

type Params struct {
	Lat   string
	Long  string
	ID    string
	Name  string
	Debug string
	Force string
}

func getParams() Params {
	var latPtr = flag.String("lat", "", "Latitude")
	var lonPtr = flag.String("lon", "", "Longitude")
	var idPtr = flag.String("id", "", "Force the Node ID")
	var namePtr = flag.String("name", "", "Force the Node Name")
	var debugPtr = flag.String("d", "", "Enable Debug")
	var force = flag.String("f", "", "Force to use the configuration, Dont update the config")

	flag.Parse()

	return Params{
		*latPtr,
		*lonPtr,
		*idPtr,
		*namePtr,
		*debugPtr,
		*force}
}
