package main

import (
    "bytes"
    "encoding/json"
    "github.com/shomali11/util/xconditions"
    "io/ioutil"
    "net/http"
    "strings"
    "time"
)

/* To ping the collection server peri*/
func postPing( startPayloadData Payload){
    transport := &http.Transport{ DisableKeepAlives: true }
    client := http.Client{Transport: transport}
    var counter int64 = 0

    type RepeatLoad struct {
        Seq     int64   `json:"seq"`
        ID      string  `json:"id"`
        Block   int64   `json:"block"`
        Debug   bool    `json:"debug"`
        Status  string  `json:"status"`
    }

    var body *bytes.Reader = nil
    // Repeat the post call to the remote server
    repeatTicker := time.NewTicker(time.Duration(conf.Heartbeat) * time.Second)
    startPayloadData.Block = getBlockHeight()
    startPayloadData.Seq = counter
    for {
        select {
        case <- repeatTicker.C:
            if counter < 1 {
                payloadBytes, _ := json.Marshal(startPayloadData)
                body = bytes.NewReader(payloadBytes)
                if conf.debug { log.Info("First Sending:", string(payloadBytes)) }
            } else {
                payloadData := RepeatLoad{
                    counter,
                    startPayloadData.ID,
                    getBlockHeight(),
                    startPayloadData.Debug,
                    "Agent:"+conf.Version+xconditions.IfThenElse(conf.mongoDB[0] != conf.mongoDB[1],"[PeeR]","[API-DB]").(string) }
                payloadBytes, _ := json.Marshal(payloadData)
                body = bytes.NewReader(payloadBytes)
                if conf.debug { log.Info("Then Sending:", string(payloadBytes)) }
            }
            postPackage := postCycle(client, body)
            if strings.Index(postPackage, "ACK") > -1 {
                counter++ //Increment only if previous package was received.
            } else if strings.Index(postPackage, "RST") > -1 {
                counter=0 //Reset to Zero to resend the initial package.
            }
        }
        time.Sleep(1000 )
    }
    transport.CloseIdleConnections()
}

func postCycle(client http.Client , body *bytes.Reader) (postCycle string) {
    defer func() {
        r := recover()
        if r != nil { log.Warning("Expected Error:", r) }
    }()

    resp, err := client.Post(conf.Server+conf.Port+conf.Path, "application/json", body)
    if err != nil {
        panic(err)
    } else {
        responseData, err := ioutil.ReadAll(resp.Body)
        if err != nil {
            postCycle = "Failed"
        } else {
            if conf.debug {
                log.Info("Response Received:", string(responseData))
            }
            postCycle = string(responseData)
        }
        defer resp.Body.Close()
    }
    return
}