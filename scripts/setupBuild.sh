#!/usr/bin/env bash

# Set the GoPath must be set (assume the deployed folder is the path)
export GOPATH=~/go
export GOBIN=$GOPATH/pkg
export GOEXE=$GOPATH/bin
if [[ $PATH != *"go"* ]]; then
    export PATH=$PATH:$GOPATH/bin
fi

# Install the Libraries for Go to build
go get github.com/pkg/errors
go get -u github.com/go-ini/ini
go get github.com/globalsign/mgo
go get -v -u github.com/op/go-logging
go get github.com/shomali11/util/xstrings
go get github.com/shomali11/util/xconditions
#go get -v -u github.com/AENCO-Global/Chain-Agent
go get -v -u gitlab.com/aenco/blockchain/Chain-Agent

# To install this package and run it, use the same as the Chain-go-sdk mentioned above.
