#!/usr/bin/env bash
echo "Run this file as Sudo"
echo "Add the Jenkins sudo "
printf "\njenkins ALL=NOPASSWD: /bin/systemctl *\n" >> /etc/sudoers.d/90-cloud-init-users
echo "Do the Symlink to the systemd monitor"
ln -s /home/jenkins/.aen/services/aen-agent.service /etc/systemd/system
echo "Set the auto start on reboot for the Agent in sudoer"
systemctl enable aen-agent
echo "---==== All Done ====----"