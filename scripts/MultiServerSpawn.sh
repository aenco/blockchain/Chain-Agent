#!/usr/bin/env bash
# Some Control
cd ..
pkill aen.agent
ps -aef | grep chain.agent | grep -v grep
cp -r ../resources/config-agent.ini.twig ../resources/config-agent.ini
sed -i '' -e "s/{{ agent.version }}/LocalRun/g" ../resources/config-agent.ini
sed -i '' -e "s/{{ deployed.log }}/..\/log\/aen.agent.log/g" ../resources/config-agent.ini
cat ../resources/config-agent.ini

./aen.agent  -lat  34.51666667  -lon  69.183333   -id T0001  -name Afghanistan         &
./aen.agent  -lat    60.116667  -lon       19.9   -id T0002  -name Aland Islands       &
./aen.agent  -lat  41.31666667  -lon  19.816667   -id T0003  -name Albania             &
./aen.agent  -lat        36.75  -lon       3.05   -id T0004  -name Algeria             &
./aen.agent  -lat -14.26666667  -lon     -170.7   -id T0005  -name American Samoa      &
./aen.agent  -lat         42.5  -lon   1.516667   -id T0006  -name Andorra             &
./aen.agent  -lat -8.833333333  -lon  13.216667   -id T0007  -name Angola              &
./aen.agent  -lat  18.21666667  -lon     -63.05   -id T0008  -name Anguilla            &
./aen.agent  -lat            0  -lon          0   -id T0009  -name Antarctica          &
./aen.agent  -lat  17.11666667  -lon     -61.85   -id T0010  -name Antigua and Barbuda &
./aen.agent  -lat -34.58333333  -lon -58.666667   -id T0011  -name Argentina           &
./aen.agent  -lat  40.16666667  -lon       44.5   -id T0012  -name Armenia             &
./aen.agent  -lat  12.51666667  -lon -70.033333   -id T0013  -name Aruba               &
./aen.agent  -lat -35.26666667  -lon 149.133333   -id T0014  -name Australia           &
./aen.agent  -lat         48.2  -lon  16.366667   -id T0015  -name Austria             &
./aen.agent  -lat  40.38333333  -lon  49.866667   -id T0016  -name Azerbaijan          &
./aen.agent  -lat  25.08333333  -lon     -77.35   -id T0017  -name Bahamas             &
./aen.agent  -lat  26.23333333  -lon  50.566667   -id T0018  -name Bahrain             &
./aen.agent  -lat   23.71666667 -lon         90.4 -id T0021  -name Bangladesh                   &
./aen.agent  -lat          13.1 -lon   -59.616667 -id T0022  -name Barbados                     &
./aen.agent  -lat          53.9 -lon    27.566667 -id T0023  -name Belarus                      &
./aen.agent  -lat   50.83333333 -lon     4.333333 -id T0024  -name Belgium                      &
./aen.agent  -lat         17.25 -lon   -88.766667 -id T0025  -name Belize                       &
./aen.agent  -lat   6.483333333 -lon     2.616667 -id T0026  -name Benin                        &
./aen.agent  -lat   32.28333333 -lon   -64.783333 -id T0027  -name Bermuda                      &
./aen.agent  -lat   27.46666667 -lon    89.633333 -id T0028  -name Bhutan                       &
./aen.agent  -lat         -16.5 -lon       -68.15 -id T0029  -name Bolivia                      &
./aen.agent  -lat   43.86666667 -lon    18.416667 -id T0020  -name Bosnia and Herzegovina       &
./aen.agent  -lat  -24.63333333 -lon         25.9 -id T0031  -name Botswana                     &
./aen.agent  -lat  -15.78333333 -lon   -47.916667 -id T0032  -name Brazil                       &
./aen.agent  -lat          -7.3 -lon         72.4 -id T0033  -name British Indian Ocean Territ  &
./aen.agent  -lat   18.41666667 -lon   -64.616667 -id T0034  -name British Virgin Islands       &
./aen.agent  -lat   4.883333333 -lon   114.933333 -id T0035  -name Brunei Darussalam            &
./aen.agent  -lat   42.68333333 -lon    23.316667 -id T0036  -name Bulgaria                     &
./aen.agent  -lat   12.36666667 -lon    -1.516667 -id T0037  -name Burkina Faso                 &
./aen.agent  -lat  -3.366666667 -lon        29.35 -id T0038  -name Burundi                      &
./aen.agent  -lat         11.55 -lon   104.916667 -id T0039  -name Cambodia                     &
./aen.agent  -lat   3.866666667 -lon    11.516667 -id T0030  -name Cameroon                     &
./aen.agent  -lat   45.41666667 -lon        -75.7 -id T0041  -name Canada                       &
./aen.agent  -lat   14.91666667 -lon   -23.516667 -id T0042  -name Cape Verde                   &
./aen.agent  -lat          19.3 -lon   -81.383333 -id T0043  -name Cayman Islands               &
./aen.agent  -lat   4.366666667 -lon    18.583333 -id T0044  -name Central African Republic     &
./aen.agent  -lat          12.1 -lon    15.033333 -id T0045  -name Chad                         &
./aen.agent  -lat        -33.45 -lon   -70.666667 -id T0046  -name Chile                        &
./aen.agent  -lat   39.91666667 -lon   116.383333 -id T0047  -name China                        &
./aen.agent  -lat  -10.41666667 -lon   105.716667 -id T0048  -name Christmas Island             &
./aen.agent  -lat  -12.16666667 -lon    96.833333 -id T0049  -name Cocos Islands                &
./aen.agent  -lat           4.6 -lon   -74.083333 -id T0040  -name Colombia                     &
./aen.agent  -lat         -11.7 -lon    43.233333 -id T0051  -name Comoros                      &
./aen.agent  -lat         -21.2 -lon  -159.766667 -id T0052  -name Cook Islands                 &
./aen.agent  -lat   9.933333333 -lon   -84.083333 -id T0053  -name Costa Rica                   &
./aen.agent  -lat   6.816666667 -lon    -5.266667 -id T0054  -name Cote d’Ivoire                &
./aen.agent  -lat          45.8 -lon           16 -id T0055  -name Croatia                      &
./aen.agent  -lat   23.11666667 -lon       -82.35 -id T0056  -name Cuba                         &
./aen.agent  -lat          12.1 -lon   -68.916667 -id T0057  -name CuraÃ§ao                     &
./aen.agent  -lat   35.16666667 -lon    33.366667 -id T0058  -name Cyprus                       &
./aen.agent  -lat   50.08333333 -lon    14.466667 -id T0059  -name Czech Republic               &
./aen.agent  -lat  -4.316666667 -lon         15.3 -id T0050  -name Democratic Republic of the   &
./aen.agent  -lat   55.66666667 -lon    12.583333 -id T0061  -name Denmark                      &
./aen.agent  -lat   11.58333333 -lon        43.15 -id T0062  -name Djibouti                     &
./aen.agent  -lat          15.3 -lon        -61.4 -id T0063  -name Dominica                     &
./aen.agent  -lat   18.46666667 -lon        -69.9 -id T0064  -name Dominican Republic           &
./aen.agent  -lat  -0.216666667 -lon        -78.5 -id T0065  -name Ecuador                      &
./aen.agent  -lat         30.05 -lon        31.25 -id T0066  -name Egypt                        &
./aen.agent  -lat          13.7 -lon        -89.2 -id T0067  -name El Salvador                  &
./aen.agent  -lat          3.75 -lon     8.783333 -id T0068  -name Equatorial Guinea            &
./aen.agent  -lat   15.33333333 -lon    38.933333 -id T0069  -name Eritrea                      &
./aen.agent  -lat   59.43333333 -lon    24.716667 -id T0060  -name Estonia                      &
./aen.agent  -lat   9.033333333 -lon         38.7 -id T0071  -name Ethiopia                     &
./aen.agent  -lat         -51.7 -lon       -57.85 -id T0072  -name Falkland Islands             &
./aen.agent  -lat            62 -lon    -6.766667 -id T0073  -name Faroe Islands                &
./aen.agent  -lat   6.916666667 -lon       158.15 -id T0074  -name Federated States of Microne  &
./aen.agent  -lat  -18.13333333 -lon   178.416667 -id T0075  -name Fiji                         &
./aen.agent  -lat   60.16666667 -lon    24.933333 -id T0076  -name Finland                      &
./aen.agent  -lat   48.86666667 -lon     2.333333 -id T0077  -name France                       &
./aen.agent  -lat  -17.53333333 -lon  -149.566667 -id T0078  -name French Polynesia             &
./aen.agent  -lat        -49.35 -lon    70.216667 -id T0079  -name French Southern and Antarct  &
./aen.agent  -lat   0.383333333 -lon         9.45 -id T0070  -name Gabon                        &
./aen.agent  -lat   41.68333333 -lon    44.833333 -id T0081  -name Georgia                      &
./aen.agent  -lat   52.51666667 -lon         13.4 -id T0082  -name Germany                      &
./aen.agent  -lat          5.55 -lon    -0.216667 -id T0083  -name Ghana                        &
./aen.agent  -lat   36.13333333 -lon        -5.35 -id T0084  -name Gibraltar                    &
./aen.agent  -lat   37.98333333 -lon    23.733333 -id T0085  -name Greece                       &
./aen.agent  -lat   64.18333333 -lon       -51.75 -id T0086  -name Greenland                    &
./aen.agent  -lat         12.05 -lon       -61.75 -id T0087  -name Grenada                      &
./aen.agent  -lat   13.46666667 -lon   144.733333 -id T0088  -name Guam                         &
./aen.agent  -lat   14.61666667 -lon   -90.516667 -id T0089  -name Guatemala                    &
./aen.agent  -lat         49.45 -lon    -2.533333 -id T0080  -name Guernsey                     &
./aen.agent  -lat           9.5 -lon        -13.7 -id T0091  -name Guinea                       &
./aen.agent  -lat         11.85 -lon   -15.583333 -id T0092  -name Guinea-Bissau                &
./aen.agent  -lat           6.8 -lon       -58.15 -id T0093  -name Guyana                       &
./aen.agent  -lat   18.53333333 -lon   -72.333333 -id T0094  -name Haiti                        &
./aen.agent  -lat             0 -lon            0 -id T0095  -name Heard Island and McDonald I  &
./aen.agent  -lat          14.1 -lon   -87.216667 -id T0096  -name Honduras                     &
./aen.agent  -lat             0 -lon            0 -id T0097  -name Hong Kong                    &
./aen.agent  -lat          47.5 -lon    19.083333 -id T0098  -name Hungary                      &
./aen.agent  -lat         64.15 -lon       -21.95 -id T0099  -name Iceland                      &
./aen.agent  -lat         28.6  -lon       77.2   -id T0101  -name India                        &
./aen.agent  -lat -6.166666667  -lon 106.816667   -id T0102  -name Indonesia                    &
./aen.agent  -lat         35.7  -lon  51.416667   -id T0103  -name Iran                         &
./aen.agent  -lat  33.33333333  -lon       44.4   -id T0104  -name Iraq                         &
./aen.agent  -lat  53.31666667  -lon  -6.233333   -id T0105  -name Ireland                      &
./aen.agent  -lat        54.15  -lon  -4.483333   -id T0106  -name Isle of Man                  &
./aen.agent  -lat  31.76666667  -lon  35.233333   -id T0107  -name Israel                       &
./aen.agent  -lat         41.9  -lon  12.483333   -id T0108  -name Italy                        &
./aen.agent  -lat           18  -lon      -76.8   -id T0109  -name Jamaica                      &
./aen.agent  -lat  35.68333333  -lon     139.75   -id T0110  -name Japan                        &
./aen.agent  -lat  49.18333333  -lon       -2.1   -id T0111  -name Jersey                       &
./aen.agent  -lat        31.95  -lon  35.933333   -id T0112  -name Jordan                       &
./aen.agent  -lat  51.16666667  -lon  71.416667   -id T0113  -name Kazakhstan                   &
./aen.agent  -lat -1.283333333  -lon  36.816667   -id T0114  -name Kenya                        &
./aen.agent  -lat -0.883333333  -lon 169.533333   -id T0115  -name Kiribati                     &
./aen.agent  -lat  42.66666667  -lon  21.166667   -id T0116  -name Kosovo                       &
./aen.agent  -lat  29.36666667  -lon  47.966667   -id T0117  -name Kuwait                       &
./aen.agent  -lat  42.86666667  -lon       74.6   -id T0118  -name Kyrgyzstan                   &
./aen.agent  -lat  17.96666667  -lon      102.6   -id T0121  -name Laos                         &
./aen.agent  -lat        56.95  -lon       24.1   -id T0122  -name Latvia                       &
./aen.agent  -lat  33.86666667  -lon       35.5   -id T0123  -name Lebanon                      &
./aen.agent  -lat -29.31666667  -lon  27.483333   -id T0124  -name Lesotho                      &
./aen.agent  -lat          6.3  -lon      -10.8   -id T0125  -name Liberia                      &
./aen.agent  -lat  32.88333333  -lon  13.166667   -id T0126  -name Libya                        &
./aen.agent  -lat  47.13333333  -lon   9.516667   -id T0127  -name Liechtenstein                &
./aen.agent  -lat  54.68333333  -lon  25.316667   -id T0128  -name Lithuania                    &
./aen.agent  -lat         49.6  -lon   6.116667   -id T0129  -name Luxembourg                   &
./aen.agent  -lat            0  -lon          0   -id T0120  -name Macau                        &
./aen.agent  -lat           42  -lon  21.433333   -id T0131  -name Macedonia                    &
./aen.agent  -lat -18.91666667  -lon  47.516667   -id T0132  -name Madagascar                   &
./aen.agent  -lat -13.96666667  -lon  33.783333   -id T0133  -name Malawi                       &
./aen.agent  -lat  3.166666667  -lon      101.7   -id T0134  -name Malaysia                     &
./aen.agent  -lat  4.166666667  -lon       73.5   -id T0135  -name Maldives                     &
./aen.agent  -lat        12.65  -lon         -8   -id T0136  -name Mali                         &
./aen.agent  -lat  35.88333333  -lon       14.5   -id T0137  -name Malta                        &
./aen.agent  -lat          7.1  -lon 171.383333   -id T0138  -name Marshall Islands             &
./aen.agent  -lat  18.06666667  -lon -15.966667   -id T0139  -name Mauritania                   &
./aen.agent  -lat       -20.15  -lon  57.483333   -id T0130  -name Mauritius                    &
./aen.agent  -lat  19.43333333  -lon -99.133333   -id T0141  -name Mexico                       &
./aen.agent  -lat           47  -lon      28.85   -id T0142  -name Moldova                      &
./aen.agent  -lat  43.73333333  -lon   7.416667   -id T0143  -name Monaco                       &
./aen.agent  -lat  47.91666667  -lon 106.916667   -id T0144  -name Mongolia                     &
./aen.agent  -lat  42.43333333  -lon  19.266667   -id T0145  -name Montenegro                   &
./aen.agent  -lat         16.7  -lon -62.216667   -id T0146  -name Montserrat                   &
./aen.agent  -lat  34.01666667  -lon  -6.816667   -id T0147  -name Morocco                      &
./aen.agent  -lat       -25.95  -lon  32.583333   -id T0148  -name Mozambique                   &
./aen.agent  -lat         16.8  -lon      96.15   -id T0149  -name Myanmar                      &
./aen.agent  -lat -22.56666667  -lon  17.083333   -id T0140  -name Namibia                      &
./aen.agent  -lat      -0.5477  -lon 166.920867   -id T0151  -name Nauru                        &
./aen.agent  -lat  27.71666667  -lon  85.316667   -id T0152  -name Nepal                        &
./aen.agent  -lat        52.35  -lon   4.916667   -id T0153  -name Netherlands                  &
./aen.agent  -lat -22.26666667  -lon     166.45   -id T0154  -name New Caledonia                &
./aen.agent  -lat        -41.3  -lon 174.783333   -id T0155  -name New Zealand                  &
./aen.agent  -lat  12.13333333  -lon     -86.25   -id T0156  -name Nicaragua                    &
./aen.agent  -lat  13.51666667  -lon   2.116667   -id T0157  -name Niger                        &
./aen.agent  -lat  9.083333333  -lon   7.533333   -id T0158  -name Nigeria                      &
./aen.agent  -lat -19.01666667  -lon-169.916667   -id T0159  -name Niue                         &
./aen.agent  -lat       -29.05  -lon 167.966667   -id T0150  -name Norfolk Island               &
./aen.agent  -lat  39.01666667  -lon     125.75   -id T0161  -name North Korea                  &
./aen.agent  -lat    35.183333  -lon  33.366667   -id T0162  -name Northern Cyprus              &
./aen.agent  -lat         15.2  -lon     145.75   -id T0163  -name Northern Mariana Islands     &
./aen.agent  -lat  59.91666667  -lon      10.75   -id T0164  -name Norway                       &
./aen.agent  -lat  23.61666667  -lon  58.583333   -id T0165  -name Oman                         &
./aen.agent  -lat  33.68333333  -lon      73.05   -id T0166  -name Pakistan                     &
./aen.agent  -lat  7.483333333  -lon 134.633333   -id T0167  -name Palau                        &
./aen.agent  -lat  31.76666667  -lon  35.233333   -id T0168  -name Palestine                    &
./aen.agent  -lat  8.966666667  -lon -79.533333   -id T0169  -name Panama                       &
./aen.agent  -lat        -9.45  -lon 147.183333   -id T0160  -name Papua New Guinea             &
./aen.agent  -lat -25.26666667  -lon -57.666667   -id T0171  -name Paraguay                     &
./aen.agent  -lat       -12.05  -lon     -77.05   -id T0172  -name Peru                         &
./aen.agent  -lat         14.6  -lon 120.966667   -id T0173  -name Philippines                  &
./aen.agent  -lat -25.06666667  -lon -130.083333   -id T0174  -name Pitcairn Islands             &
./aen.agent  -lat        52.25  -lon         21   -id T0175  -name Poland                       &
./aen.agent  -lat  38.71666667  -lon  -9.133333   -id T0176  -name Portugal                     &
./aen.agent  -lat  18.46666667  -lon -66.116667   -id T0177  -name Puerto Rico                  &
./aen.agent  -lat  25.28333333  -lon  51.533333   -id T0178  -name Qatar                        &
./aen.agent  -lat        -4.25  -lon  15.283333   -id T0179  -name Republic of Congo            &
./aen.agent  -lat  44.43333333  -lon       26.1   -id T0170  -name Romania                      &
./aen.agent  -lat        55.75  -lon       37.6   -id T0181  -name Russia                       &
./aen.agent  -lat        -1.95  -lon      30.05   -id T0182  -name Rwanda                       &
./aen.agent  -lat  17.88333333  -lon     -62.85   -id T0183  -name Saint Barthelemy             &
./aen.agent  -lat -15.93333333  -lon  -5.716667   -id T0184  -name Saint Helena                 &
./aen.agent  -lat         17.3  -lon -62.716667   -id T0185  -name Saint Kitts and Nevis        &
./aen.agent  -lat           14  -lon        -61   -id T0186  -name Saint Lucia                  &
./aen.agent  -lat      18.0731  -lon   -63.0822   -id T0187  -name Saint Martin                 &
./aen.agent  -lat  46.76666667  -lon -56.183333   -id T0188  -name Saint Pierre and Miquelon    &
./aen.agent  -lat  13.13333333  -lon -61.216667   -id T0189  -name Saint Vincent and the Grena  &
./aen.agent  -lat -13.81666667  -lon-171.766667   -id T0180  -name Samoa                        &
./aen.agent  -lat  43.93333333  -lon  12.416667   -id T0191  -name San Marino                   &
./aen.agent  -lat  0.333333333  -lon   6.733333   -id T0192  -name Sao Tome and Principe        &
./aen.agent  -lat        24.65  -lon       46.7   -id T0193  -name Saudi Arabia                 &
./aen.agent  -lat  14.73333333  -lon -17.633333   -id T0194  -name Senegal                      &
./aen.agent  -lat  44.83333333  -lon       20.5   -id T0195  -name Serbia                       &
./aen.agent  -lat -4.616666667  -lon      55.45   -id T0196  -name Seychelles                   &
./aen.agent  -lat  8.483333333  -lon -13.233333   -id T0197  -name Sierra Leone                 &
./aen.agent  -lat  1.283333333  -lon     103.85   -id T0198  -name Singapore                    &
./aen.agent  -lat  18.01666667  -lon -63.033333   -id T0199  -name Sint Maarten                 &